FROM python:3.9-slim-buster

WORKDIR /app
COPY . .

RUN pip install flask

CMD ["python", "app.py"]

